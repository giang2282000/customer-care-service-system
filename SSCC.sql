-- ########## create database query
-- CREATE TABLE Campaign (
--     id varchar(16) NOT NULL PRIMARY KEY,
-- 	campaign_name varchar(16),
-- 	campaign_script varchar(128),
-- 	campaign_start_time TIMESTAMP,
-- 	campaign_end_time TIMESTAMP,
-- 	campaign_create_time TIMESTAMP,
-- 	staff_id varchar Array
-- );
-- CREATE TABLE CareStaff (
--     id varchar(16) NOT NULL PRIMARY KEY,
-- 	staff_name varchar(16),
-- 	staff_detail varchar(256)
-- );
-- alter table Campaign
-- 	ADD COLUMN creator_id varchar(16) ;
-- alter table Campaign
-- 	add CONSTRAINT FK_CareStaff FOREIGN KEY (creator_id) REFERENCES CareStaff(id);
	
-- CREATE TABLE Call (
--     id varchar(16) NOT NULL PRIMARY KEY,
-- 	start_time TIMESTAMP,
-- 	end_time TIMESTAMP,
-- 	call_result varchar(32),
-- 	campaign_id varchar(16) not null,
-- 	staff_id varchar(16) not null,
-- 	CONSTRAINT FK_campaign FOREIGN KEY (campaign_id) REFERENCES Campaign(id),
-- 	CONSTRAINT FK_staff_call FOREIGN KEY (staff_id) REFERENCES CareStaff(id),
-- 	customer_id varchar(16) not null
-- );

-- CREATE TABLE Customer (
--     id varchar(16) NOT NULL PRIMARY KEY,
-- 	customer_name varchar(16),
-- 	customer_detail varchar(256)
-- );
-- alter table Call
-- 	add CONSTRAINT FK_customer FOREIGN KEY (customer_id) REFERENCES Customer(id)

	
	
-- ########## example query specific result
-- ###### đếm số call trong các campaign
-- SELECT CAMPAIGN.ID,
-- 	COUNT(CALL.ID)
-- FROM CALL,
-- 	CAMPAIGN
-- WHERE CAMPAIGN.ID = CALL.CAMPAIGN_ID
-- GROUP BY CAMPAIGN.ID

-- -- ###### đếm số customer trong các campaign
-- SELECT CAMPAIGN.ID,
-- 	COUNT(DISTINCT CALL.CUSTOMER_ID)
-- FROM CALL,
-- 	CAMPAIGN
-- WHERE CAMPAIGN.ID = CALL.CAMPAIGN_ID
-- GROUP BY CAMPAIGN.ID

-- -- -- ###### đếm số customer của các staff trong các campaign
-- WITH A AS
-- 	(SELECT CALL.STAFF_ID,
-- 			COUNT(DISTINCT CALL.CUSTOMER_ID) as customer_counter
-- 		FROM CALL
-- 		GROUP BY CALL.STAFF_ID),
-- 	B AS
-- 	(SELECT CAMPAIGN.ID,
-- 			CALL.STAFF_ID
-- 		FROM CAMPAIGN,
-- 			CALL
-- 		WHERE CAMPAIGN.ID = CALL.CAMPAIGN_ID)
-- SELECT b.id as campaign_id, a.staff_id, a.customer_counter
-- FROM B
-- LEFT JOIN A ON A.STAFF_ID = B.STAFF_ID

-- -- ###### tìm staff với số cuộc gọi nhiều nhất
-- SELECT CALL.STAFF_ID,
-- 	COUNT(CALL.ID)
-- FROM CALL,
-- 	CARESTAFF
-- WHERE CALL.STAFF_ID = CARESTAFF.ID
-- GROUP BY CALL.STAFF_ID
-- ORDER BY COUNT(CALL.ID) FETCH FIRST ROW ONLY







-- -- ########## generate id
-- 	INSERT INTO carestaff(id) VALUES (generate_series(1, 10));
-- -- -- ########## generate name
-- with gnn as(
-- 	SELECT 
--     arrays.firstnames[s.a % ARRAY_LENGTH(arrays.firstnames,1) + 1] AS firstname,
--     substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ' from s.a%26+1 for 1) AS middlename,
--     arrays.lastnames[s.a % ARRAY_LENGTH(arrays.lastnames,1) + 1] AS lastname
-- FROM     generate_series(1,300) AS s(a) -- number of names to generate 
-- CROSS JOIN(
--     SELECT ARRAY[
--     'Adam','Bill','Bob','Calvin','Donald','Dwight','Frank','Fred','George','Howard',
--     'James','John','Jacob','Jack','Martin','Matthew','Max','Michael',
--     'Paul','Peter','Phil','Roland','Ronald','Samuel','Steve','Theo','Warren','William',
--     'Abigail','Alice','Allison','Amanda','Anne','Barbara','Betty','Carol','Cleo','Donna',
--     'Jane','Jennifer','Julie','Martha','Mary','Melissa','Patty','Sarah','Simone','Susan'
--     ] AS firstnames,
--     ARRAY[
--         'Matthews','Smith','Jones','Davis','Jacobson','Williams','Donaldson','Maxwell','Peterson','Stevens',
--         'Franklin','Washington','Jefferson','Adams','Jackson','Johnson','Lincoln','Grant','Fillmore','Harding','Taft',
--         'Truman','Nixon','Ford','Carter','Reagan','Bush','Clinton','Hancock'
--     ] AS lastnames
-- ) AS arrays
-- ), bnn as(
-- select concat(gnn.firstname,' ', gnn.lastname ) as name 
-- from gnn 
-- fetch first 10 rows only
-- )

select * from bnn, carestaff where
insert into carestaff(id) values(CAST(generate_series(1, 10) as varchar(16)))
-- update carestaff
-- set staff_name = bnn.name
-- FROM bnn 
-- where carestaff.staff_name!=bnn.name


-- delete from carestaff

